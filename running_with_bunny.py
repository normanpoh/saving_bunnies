'''

Running with Bunnies
====================

You and the bunny workers need to get out of this collapsing death trap of a space station -- and fast! Unfortunately, some of the bunnies have been weakened by their long work shifts and can't run very fast. Their friends are trying to help them, but this escape would go a lot faster if you also pitched in. The defensive bulkhead doors have begun to close, and if you don't make it through in time, you'll be trapped! You need to grab as many bunnies as you can and get through the bulkheads before they close.

The time it takes to move from your starting point to all of the bunnies and to the bulkhead will be given to you in a square matrix of integers. Each row will tell you the time it takes to get to the start, first bunny, second bunny, ..., last bunny, and the bulkhead in that order. The order of the rows follows the same pattern (start, each bunny, bulkhead). The bunnies can jump into your arms, so picking them up is instantaneous, and arriving at the bulkhead at the same time as it seals still allows for a successful, if dramatic, escape. (Don't worry, any bunnies you don't pick up will be able to escape with you since they no longer have to carry the ones you did pick up.) You can revisit different spots if you wish, and moving to the bulkhead doesn't mean you have to immediately leave -- you can move to and from the bulkhead to pick up additional bunnies if time permits.

In addition to spending time traveling between bunnies, some paths interact with the space station's security checkpoints and add time back to the clock. Adding time to the clock will delay the closing of the bulkhead doors, and if the time goes back up to 0 or a positive number after the doors have already closed, it triggers the bulkhead to reopen. Therefore, it might be possible to walk in a circle and keep gaining time: that is, each time a path is traversed, the same amount of time is used or added.

Write a function of the form solution(times, time_limit) to calculate the most bunnies you can pick up and which bunnies they are, while still escaping through the bulkhead before the doors close for good. If there are multiple sets of bunnies of the same size, return the set of bunnies with the lowest worker IDs (as indexes) in sorted order. The bunnies are represented as a sorted list by worker ID, with the first bunny being 0. There are at most 5 bunnies, and time_limit is a non-negative integer that is at most 999.

For instance, in the case of
[
  [0, 2, 2, 2, -1],  # 0 = Start
  [9, 0, 2, 2, -1],  # 1 = Bunny 0
  [9, 3, 0, 2, -1],  # 2 = Bunny 1
  [9, 3, 2, 0, -1],  # 3 = Bunny 2
  [9, 3, 2, 2,  0],  # 4 = Bulkhead
]
and a time limit of 1, the five inner array rows designate the starting point, bunny 0, bunny 1, bunny 2, and the bulkhead door exit respectively. You could take the path:

Start End Delta Time Status
    -   0     -    1 Bulkhead initially open
    0   4    -1    2
    4   2     2    0
    2   4    -1    1
    4   3     2   -1 Bulkhead closes
    3   4    -1    0 Bulkhead reopens; you and the bunnies exit

With this solution, you would pick up bunnies 1 and 2. This is the best combination for this space station hallway, so the solution is [1, 2].

Languages
=========

To provide a Java solution, edit Solution.java
To provide a Python solution, edit solution.py

Test cases
==========
Your code should pass the following test cases.
Note that it may also be run against hidden test cases not shown here.

-- Java cases --
Input:
Solution.solution({{0, 1, 1, 1, 1}, {1, 0, 1, 1, 1}, {1, 1, 0, 1, 1}, {1, 1, 1, 0, 1}, {1, 1, 1, 1, 0}}, 3)
Output:
    [0, 1]

Input:
Solution.solution({{0, 2, 2, 2, -1}, {9, 0, 2, 2, -1}, {9, 3, 0, 2, -1}, {9, 3, 2, 0, -1}, {9, 3, 2, 2, 0}}, 1)
Output:
    [1, 2]

-- Python cases --
Input:
solution.solution([[0, 2, 2, 2, -1], [9, 0, 2, 2, -1], [9, 3, 0, 2, -1], [9, 3, 2, 0, -1], [9, 3, 2, 2, 0]], 1)
Output:
    [1, 2]

Input:
solution.solution([[0, 1, 1, 1, 1], [1, 0, 1, 1, 1], [1, 1, 0, 1, 1], [1, 1, 1, 0, 1], [1, 1, 1, 1, 0]], 3)
Output:
    [0, 1]

'''



class BunnyRescue:
    def __init__(self, adj_matrix):
        self.adj_matrix = adj_matrix

        # Create a header (list) like 'start', '0', 1', '2', 'bulkhead'
        header = ['start']
        for i in range(len(self.adj_matrix) - 2):
            header.append(f"bunny {i}")
        header.append('bulkhead')
        self.header = header

        self.position = dict(zip(self.header, range(len(self.adj_matrix))))
        self.name = dict(zip(range(len(self.adj_matrix)), self.header))

        # Initially all bunnies have the same weight so we don't have preference
        # Later on, we want to reduce
        self.h = {}
        for i in range(len(self.adj_matrix)):
            self.h[i] = 1
        return

    def get_time(self, curr_pos, next_candidiate_pos):
        return self.adj_matrix[curr_pos][next_candidiate_pos]

    def mark_bunny_visit(self, pos:int=None):
        # Remember that pos - 1 is the first bunny (i.e., bunny 0)
        if pos is None:
            self.visited_bunny = [False] * (len(self.adj_matrix) - 2)
        else:
            if pos >=1 and pos < len(self.adj_matrix)-1:
                self.visited_bunny[pos - 1] = True
                self.h[pos] -= 0.1 # bunny that has already been visited is given a lower value
        return

    def is_bunny_visited(self, pos):
        # Exclue the first and last case
        if pos == 0 or pos >= len(self.adj_matrix) - 1:
            return False
        return self.visited_bunny[pos - 1]

    def pick_next_candidates(self, pos):
        index_list = list(range(len(self.adj_matrix)))
        adj_matrix_minus_1 = self.adj_matrix[pos][:]
        del adj_matrix_minus_1[pos]
        del index_list[pos]
        min_index = argmin(adj_matrix_minus_1)
        next_candidate_pos = [index_list[x] for x in min_index]
        return next_candidate_pos

    def solve(self, time_limit=1):
        g = time_limit
        curr_state ='start'
        self.mark_bunny_visit(None) #initialise the bunny visit list

        # open_list = set([])

        # Stop criteria are
        times_up = False # either the time is up
        patience = 5 # run out of patience

        while not all(self.visited_bunny) and not times_up and not (patience < 0):
            curr_pos = self.position[curr_state]

            # Pick next candidate positions except itself
            next_candidate_state = self.pick_next_candidates(curr_pos)

            # If there are several candidates, we may have to push to the open_list
            if len(next_candidate_state) == 1:
                # The next position is the accepted position
                next_pos = next_candidate_state[0]
                winning_pos = next_pos # it is also the winning position
                f = g - self.get_time(curr_pos, next_pos) + self.h[next_pos]
                winning_f = f
            else:
                # Pick the best candidates
                f_list = [g - self.get_time(curr_pos, next_pos) + self.h[next_pos] for next_pos in next_candidate_state]
                winning_pos = [next_candidate_state[x] for x in argmax(f_list)][0] #pick the first element
                winning_f = max(f_list)

            if self.is_bunny_visited(winning_pos):
                patience -= 1 #we may soon run out of patience if we keep visiting the same bunny!

            self.mark_bunny_visit(winning_pos)
            g = g - self.get_time(curr_pos, winning_pos)
            curr_state = self.name[winning_pos]
            print(f"Hopped to {curr_state} with {g}")

        # print the visited bunny
        visited_bunnies = [i for v, i in zip(self.visited_bunny,range(0,len(self.adj_matrix)-2)) if v]
        print(f"Visited bunnies: {visited_bunnies}")
        return

def argmin(list_):
    # Return indexes with minium values
    min_ = min(list_)
    return [i for i, v in enumerate(list_) if v == min_]


def argmax(list_):
    # Return indexes with minium values
    max_ = max(list_)
    return [i for i, v in enumerate(list_) if v == max_]

adj_matrix = [
  [0, 2, 2, 2, -1],  # 0 = Start
  [9, 0, 2, 2, -1],  # 1 = Bunny 0
  [9, 3, 0, 2, -1],  # 2 = Bunny 1
  [9, 3, 2, 0, -1],  # 3 = Bunny 2
  [9, 3, 2, 2,  0],  # 4 = Bulkhead
]


self = bunny = BunnyRescue(adj_matrix)
time_limit = 1
bunny.solve(1)

adj_matrix = [[0, 1, 1, 1, 1],
 [1, 0, 1, 1, 1],
 [1, 1, 0, 1, 1],
 [1, 1, 1, 0, 1],
 [1, 1, 1, 1, 0]]
self = bunny = BunnyRescue(adj_matrix)
bunny.solve(3)